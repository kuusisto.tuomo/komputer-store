// Header & section references
const laptopsHeaderElement = document.getElementById("laptops")
const laptopFeaturesElement = document.getElementById("laptop-feature-list")
const laptopTitleElement = document.getElementById("laptop-title")
const laptopDescriptionElement = document.getElementById("laptop-description")
const laptopPriceElement = document.getElementById("laptop-price")

// Balance value references
const bankBalanceElement = document.getElementById("currentBankBalance")
const cashBalanceElement = document.getElementById("currentCashBalance")
const outstandingLoanParent = document.getElementById("outstanding-loan-parent")

// Button references
const loanButtonElement = document.getElementById("loan-button")
const bankButtonElement = document.getElementById("bank-button")
const workButtonElement = document.getElementById("work-button")
const buyButtonElement = document.getElementById("buy-button")

// Constant variables
const wage = 100.0          // Salary that is paid for working
const loanDeduction = 0.1   // % of salary that is automatically deducted to pay for an outstanding loan

// Other variables
let laptops = []
let currentBankBalance = 500.0  // Current bank balance
let outstandingLoan = 0.0       // Current amount of loan
let currentCashBalance = 0.0    // Current cash that is not part of the bank balance
let selectedLaptop              // Currently viewed laptop

// Turns a number into proper currency format (currently set as Euros in Finnish format)
function convertToCurrency(value){
    return new Intl.NumberFormat('fi-FI', {style: "currency", currency: "EUR"}).format(value)
}

/* BANK SECTION */
bankBalanceElement.innerText = convertToCurrency(currentBankBalance)
cashBalanceElement.innerText = convertToCurrency(currentCashBalance)

const handleLoan = () => {
    console.log("Loan button clicked")

    // First check if there is any outstanding loan. Two loans at once is not permitted.
    if(outstandingLoan > 0){
        console.log(`Loan of ${convertToCurrency(outstandingLoan)} already exists, cannot receive a new loan before paying up!`)
        alert(`A loan of ${convertToCurrency(outstandingLoan)} exists already, pay up before asking for more!`)
    } else{
        // Prompt for the loan amount.
        const loanedAmount = prompt("Please enter the loan amount you wish to receive: ")
        console.log(`Trying to loan ${convertToCurrency(loanedAmount)}.`)

        // Make sure that whatever was inserted is a proper value.
        if(isNaN(loanedAmount) || loanedAmount < 0.01){
            alert(`"${loanedAmount}" is not a proper value, make sure the input is a numerical value with the minimum of 0.01!`)
        } else{
            // Make sure the requested loan is not greater than double the bank balance. Otherwise, decline the loan.
            if(loanedAmount > 2 * currentBankBalance){
                console.log(`Tried to loan an amount that is ${loanedAmount / currentBankBalance} times the current balance, which is more than double so declining.`)
                alert(`I'm afraid a loan of ${convertToCurrency(loanedAmount)} is too much. The best we could do is ${convertToCurrency(2 * currentBankBalance)}.`)
            } else{
                console.log(`Trying to loan an amount that is ${loanedAmount / currentBankBalance} times the current balance, which is okay.`)
                currentBankBalance += parseFloat(loanedAmount)
                outstandingLoan += parseFloat(loanedAmount)
                RefreshBankTexts()
            }
        }
    }
}

// Refreshes values in bank and work section
function RefreshBankTexts(){
    bankBalanceElement.innerText = convertToCurrency(currentBankBalance)
    let repayloanElement = document.getElementById('repay-button')

    // Check if outstanding loan element exists before updating it. If it doesn't exist, generate it.
    if(outstandingLoan > 0){
        let loanTitle = document.getElementById('outstanding-loan-title')
        if(loanTitle == null){
            loanTitle = document.createElement('td')
            loanTitle.id = "outstanding-loan-title"
            loanTitle.classList.add("information")
            outstandingLoanParent.appendChild(loanTitle)
        }

        let loanValue = document.getElementById('outstanding-loan-value')
        if(loanValue == null){
            loanValue = document.createElement('td')
            loanValue.id = "outstanding-loan-value"
            loanValue.classList.add("value")
            outstandingLoanParent.appendChild(loanValue)
        }

        loanTitle.innerText = `Outstanding loan`
        loanValue.innerText = `${convertToCurrency(outstandingLoan)}`

        let workSection = document.getElementById('work-buttons')
        // Create repay button in case it doesn't exist yet
        if(repayloanElement == null){
            repayloanElement = document.createElement('button')
            repayloanElement.id = 'repay-button'
            repayloanElement.innerText = "Repay loan"
            workSection.appendChild(repayloanElement)
            repayloanElement.addEventListener("click", handleRepay)
        }
    } else{
        // Remove repay button in case there is no outstanding loan.
        if(repayloanElement != null){
            repayloanElement.remove()
        }

        let loanTitle = document.getElementById('outstanding-loan-title')
        if(loanTitle != null){
            loanTitle.remove()
        }

        let loanValue = document.getElementById('outstanding-loan-value')
        if(loanValue != null){
            loanValue.remove()
        }
    }
}

/* WORK SECTION */
const handleWork = () => {
    console.log(`Work button clicked. Received ${convertToCurrency(wage)}`)
    currentCashBalance += wage
    cashBalanceElement.innerText = convertToCurrency(currentCashBalance)
}

const handleBank = () => {
    console.log(`Bank button clicked. Current cash: ${convertToCurrency(currentCashBalance)}`)

    if(outstandingLoan > 0){
        const currentCash = currentCashBalance
        // Make sure not to deduct more than the outstanding loan amount
        const deductedLoan = Math.min(loanDeduction * currentCash, outstandingLoan)
        outstandingLoan -= deductedLoan
        currentBankBalance += currentCash - deductedLoan
        // const outstandingLoanElement = document.createElement('p')
        console.log(`Deducted ${convertToCurrency(deductedLoan)} to pay for outstanding loan. ${convertToCurrency(currentCash - deductedLoan)} transferred to bank account.`)
    } else{
        console.log("No outstanding loan, transferring all cash to bank account.")
        currentBankBalance += currentCashBalance
    }

    currentCashBalance = 0
    cashBalanceElement.innerText = convertToCurrency(currentCashBalance)
    RefreshBankTexts()
}

const handleRepay = () => {
    console.log(`Repay button clicked. Current cash: ${convertToCurrency(currentCashBalance)}`)

    if(outstandingLoan > 0){
        const currentCash = currentCashBalance
        // Make sure not to deduct more than the outstanding loan amount
        const deductedLoan = Math.min(currentCash, outstandingLoan)
        outstandingLoan -= deductedLoan
        currentBankBalance += currentCash - deductedLoan
        console.log(`Using cash to pay for loan, spent ${convertToCurrency(deductedLoan)}. ${convertToCurrency(currentCash - deductedLoan)} transferred to bank account.`)
    } else{
        console.log("No outstanding loan, transferring all cash to bank account.")
        currentBankBalance += currentCashBalance
    }

    currentCashBalance = 0
    cashBalanceElement.innerText = convertToCurrency(currentCashBalance)
    RefreshBankTexts()
}

// Fetch laptop data from url
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(r => r.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToCatalogue(laptops));

// Called once when laptop list has been fetched
const addLaptopsToCatalogue = (laptops) => {
    laptops.forEach(x => addLaptopToCatalogue(x))
    selectedLaptop = laptops[0]
    RefreshLaptopFeatures(laptops[0])
}

// Keys that are required from each laptop to allow showing it in the catalogue
let requiredKeys = ["id", "title", "description", "specs", "price", "stock", "active", "image"]

// Initial generation of laptop dropdown list
const addLaptopToCatalogue = (laptop) => {
    // Make sure all required keys exist before adding this laptop to list
    const hasAllKeys = requiredKeys.every(element => laptop.hasOwnProperty(element))
    console.log(`Has all elements: ${hasAllKeys}`)
    if(hasAllKeys){
        // Only list laptops that are marked as "active"
        const isActive = laptop["active"]
        if(isActive){
            const laptopElement = document.createElement("option")
            laptopElement.value = laptop.id
            laptopElement.appendChild(document.createTextNode(laptop.title))
            laptopsHeaderElement.appendChild(laptopElement)
        }

        // TODO: Handle out of stock
    }
}

// Called when laptop is changed from the dropdown list
const handleLaptopChange = e => {
    selectedLaptop = laptops[e.target.selectedIndex]
    RefreshLaptopFeatures(selectedLaptop)
}

// Refreshes text list of laptop features
const RefreshLaptopFeatures = (laptop) => {
    const currentFeatures = document.querySelectorAll('.laptop-feature')
    currentFeatures.forEach(feature => feature.remove())
    const features = laptop["specs"]
    features.forEach(element => {
        let laptopFeature = document.createElement('ul')
        laptopFeature.className = 'laptop-feature'
        laptopFeaturesElement.appendChild(laptopFeature)
        laptopFeature.innerText = element.toString()
    })

    laptopTitleElement.innerText = laptop.title
    laptopDescriptionElement.innerText = laptop.description
    laptopPriceElement.innerText = convertToCurrency(laptop["price"])
    let imageUrl = "https://noroff-komputer-store-api.herokuapp.com/" + laptop.image
    loadLaptopImage(imageUrl)
}

// Fetch image for laptop
async function loadLaptopImage(url){
    const backupImage = "https://noroff-komputer-store-api.herokuapp.com/assets/images/1.png"

    const options = {
        method: "GET"
    }

    let response = fetch(url, options)
        .then((r) => {
            if(r.ok){
                console.log("Response ok")
                setLaptopImage(url)
            } else{
                console.log("Response not ok")
                // Test different image format in case url didn't work
                let altUrl = url.replace(".jpg", ".png")
                response = fetch(altUrl, options)
                    .then((r) => {
                        if(r.ok){
                            console.log("Alt url response ok")
                            setLaptopImage(altUrl)
                        } else{
                            console.log("Alt url response not ok")
                        }
                    })
            }
        })
}

function setLaptopImage(url){
    const image = document.getElementById("laptop-image")
    image.src = url
}

// Laptop buy button functionality
const handleLaptopBuy = () => {
    console.log(`Buy button clicked. Current money in bank: ${convertToCurrency(currentBankBalance)}`)

    if(currentBankBalance < selectedLaptop["price"]){
        alert("Not enough money to purchase this laptop.")
    } else{
        currentBankBalance -= selectedLaptop["price"]
        RefreshBankTexts()
        alert(`Purchase successful, you are now a proud owner of ${selectedLaptop.title}!`)
    }
}

// Button event listeners
laptopsHeaderElement.addEventListener("change", handleLaptopChange)
buyButtonElement.addEventListener("click", handleLaptopBuy)
loanButtonElement.addEventListener("click", handleLoan)
workButtonElement.addEventListener("click", handleWork)
bankButtonElement.addEventListener("click", handleBank)